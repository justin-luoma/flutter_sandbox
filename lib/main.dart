import 'package:flutter/material.dart';
import 'package:one/features/counter/counter_provider.dart';
import 'package:one/home.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const OneApp());
}

class OneApp extends StatelessWidget {
  const OneApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var app = MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const Home(title: 'Flutter Demo Home Page'),
    );

    return ChangeNotifierProvider<CounterProvider>(
      create: (BuildContext context) => CounterProvider(),
      child: app,
    );
  }
}
