import 'package:flutter/material.dart';
import 'package:one/features/counter/counter_provider.dart';
import 'package:one/features/counter/counter_view.dart';
import 'package:provider/src/provider.dart';

class Page1 extends StatelessWidget {
  const Page1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Page 1'),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          const CounterView(),
          ElevatedButton(
            onPressed: () => {Navigator.pop(context)},
            child: const Text('Go Back'),
          ),
          ElevatedButton(
            onPressed: () =>
                Provider.of<CounterProvider>(context, listen: false)
                    .decrementCount(),
            child: const Text('Decrease Count'),
          ),
        ],
      )),
    );
  }
}
