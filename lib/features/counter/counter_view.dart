import 'package:flutter/material.dart';
import 'package:one/features/counter/counter_provider.dart';
import 'package:provider/provider.dart';

class CounterView extends StatelessWidget {
  const CounterView({Key? key}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<CounterProvider>(builder: (context, provider, child) {
      return Text(
        '${provider.count}',
        style: Theme.of(context).textTheme.headline4,
      );
    });
  }
}
