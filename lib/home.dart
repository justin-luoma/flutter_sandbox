import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:one/features/counter/counter_provider.dart';
import 'package:one/features/counter/counter_view.dart';
import 'package:one/page1.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  const Home({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    var fabBackgroundColor = Theme.of(context).colorScheme.secondaryVariant;

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text('Button pressed:'),
            const CounterView(),
            ElevatedButton(
                onPressed: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const Page1(),
                      ),
                    ),
                child: const Text('Go to page 1')),
          ],
        ),
      ),
      floatingActionButton: SpeedDial(
        icon: Icons.menu,
        backgroundColor: fabBackgroundColor,
        children: [
          SpeedDialChild(
            label: 'Decrement',
            child: FloatingActionButton(
              onPressed: () =>
                  Provider.of<CounterProvider>(context, listen: false)
                      .decrementCount(),
              tooltip: 'Decrement',
              child: const Icon(Icons.remove),
              backgroundColor: fabBackgroundColor,
            ),
          ),
          SpeedDialChild(
            label: 'Increment',
            child: FloatingActionButton(
              onPressed: () =>
                  Provider.of<CounterProvider>(context, listen: false)
                      .incrementCount(),
              tooltip: 'Increment',
              child: const Icon(Icons.add),
              backgroundColor: fabBackgroundColor,
            ),
          ),
        ],
      ),
    );
  }
}
